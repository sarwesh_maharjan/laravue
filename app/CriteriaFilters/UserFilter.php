<?php

namespace App\CriteriaFilters;

use Illuminate\Database\Eloquent\Builder;

/**
 * Class BeneficiaryFilter.
 */
class UserFilter extends BaseFilters
{
    /**
     * @param string $name
     * @return Builder
     */
    public function search(string $name): Builder
    {
//        return $this->query->where('name', 'LIKE', "%$name%")
//            ->whereHas('posts', function (Builder $query) use ($name) {
//                $query->where('title', 'LIKE', "%$name%")
//                    ->orWhere('description', 'LIKE', "%$name%");
//            });

        return $this->query->whereHas('posts', function (Builder $query) use ($name) {
            $query->where('title', 'LIKE', "%$name%")
                ->orWhere('description', 'LIKE', "%$name%");
        })->orWhere('name', 'LIKE', "%$name%");
    }

    /**
     * @param int $num
     * @return Builder
     */
    public function limit(int $num): Builder
    {
        return $this->query->limit($num);
    }

//    public function page(int $num): Builder
//    {
//        return $this->query->paginate($num);
//    }
}
