<?php


namespace App\Services;




use App\Http\Resources\UserResource;
use App\Repository\UserRepository;
use Exception;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class PluginUserService
{
    /**
     * @var UserRepository
     */
    protected $repository;

    /**
     * PluginUserService constructor.
     *
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $filters
     * @param array $with
     * @return AnonymousResourceCollection
     * @throws Exception
     */
    public function all(array $filters, array $with = []): AnonymousResourceCollection
    {
        try {
            return UserResource::collection($this->repository->with($with)->filterBy($filters)->latest()->get());
        } catch (Exception $e) {
            throw new Exception($e);
        }
    }

    /**
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    public function create(array $data)
    {
        try {
            return $this->repository->create($data);
        } catch (Exception $e) {
            throw new Exception($e);
        }
    }

    /**
     * @param  $userID
     * @return mixed
     * @throws Exception
     */
    public function show($userID)
    {
        try {
            return $this->repository->find($userID);
        } catch (Exception $e) {
            throw new Exception($e);
        }
    }

    /**
     * @param $data
     * @param $user
     * @return mixed
     * @throws Exception
     */
    public function edit($data, $user)
    {
        try {
            return $this->repository->update(array_filter($data),$user->id);
        } catch (Exception $e) {
            throw new Exception($e);
        }
    }

    /**
     * @param $id
     * @return int
     * @throws Exception
     */
    public function delete($id): int
    {
        try {
            return $this->repository->delete($id);
        } catch (Exception $e) {
            throw new Exception($e);
        }
    }

}
