<?php

namespace App\Services;

use App\Http\Resources\UserResource;
use App\Models\User;
use Exception;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class UserServices
{
    /**
     * @param array $filters
     * @param array $with
     * @return AnonymousResourceCollection
     * @throws Exception
     */
    public function all(array $filters, array $with = []): AnonymousResourceCollection
    {
        try {
            return UserResource::collection(User::with($with)->filterBy($filters)->latest()->get());
        } catch (Exception $e) {
            throw new Exception($e);
        }
    }

    /**
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    public function create(array $data)
    {
        try {
            return User::create($data);
        } catch (Exception $e) {
            throw new Exception($e);
        }
    }

    /**
     * @param  $userID
     * @return mixed
     * @throws Exception
     */
    public function show($userID)
    {
        try {
            return User::findOrFail($userID);
        } catch (Exception $e) {
            throw new Exception($e);
        }
    }

    /**
     * @param $data
     * @param $user
     * @return mixed
     * @throws Exception
     */
    public function edit($data, $user)
    {
        try {
            return $user->update(array_filter($data));
        } catch (Exception $e) {
            throw new Exception($e);
        }
    }

    /**
     * @param $id
     * @return int
     * @throws Exception
     */
    public function delete($id): int
    {
        try {
            return User::destroy($id);
        } catch (Exception $e) {
            throw new Exception($e);
        }
    }
}
