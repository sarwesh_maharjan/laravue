<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = $this->getGenericRule();
        if ($this->getMethod() == 'POST') {
            $rules += ['password'=> ['required', 'confirmed']];
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function validated(): array
    {
        return array_merge(parent::validated(), [
            'password' => bcrypt($this->password),
        ]);
    }

    /**
     * @return array
     */
    private function getGenericRule(): array
    {
        return[
            'name'=>'required',
            'email'=>['required', 'email', Rule::unique('users', 'email')->ignore($this->user)],
        ];
    }
}
