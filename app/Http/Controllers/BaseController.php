<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;

/**
 * Class BaseController.
 */
class BaseController extends Controller
{
    /**
     * @param string $message
     * @param int $status
     * @return JsonResponse
     */
    public function sendErrorMessage(string $message, int $status = 404): JsonResponse
    {
        return response()->json([
            'success' => false,
            'message' => $message,
            'status' => $status,
        ]);
    }

    /**
     * @param $data
     * @param string $message
     * @param int $status
     * @return JsonResponse
     */
    public function sendSuccessMessage($data, string $message, int $status = 200): JsonResponse
    {
        return response()->json([
            'success' => true,
            'message' => $message,
            'data' => $data,
            'status' => $status,
        ]);
    }

    /**
     * @param \Exception $e
     */
    public function setLog(\Exception $e)
    {
        logger()->error($e->getMessage());
    }
}
