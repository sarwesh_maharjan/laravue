<?php

namespace App\Http\Controllers\Api\PluginUser;

use App\Events\UserHasBeenRegister;
use App\Http\Controllers\BaseController;
use App\Http\Requests\StoreUserRequest;
use App\Models\User;
use App\Services\PluginUserService;
use App\Services\UserServices;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class PluginUserController extends BaseController
{  /**
 * @var UserServices
 */
    protected $pluginUserServices;

    /**
     * UserController constructor.
     *
     * @param PluginUserService $pluginUserServices
     */
    public function __construct(PluginUserService $pluginUserServices)
    {
        $this->pluginUserServices = $pluginUserServices;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse|AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        try {
            return $this->pluginUserServices->all($request->all(), ['posts']);
        } catch (\Exception $e) {
            $this->setLog($e);

            return $this->sendErrorMessage($e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreUserRequest $request
     * @return JsonResponse|AnonymousResourceCollection
     */
    public function store(StoreUserRequest $request)
    {
        try {
            $createdUser = $this->pluginUserServices->create($request->validated());
            event(new UserHasBeenRegister($createdUser));

            return $createdUser;
        } catch (\Exception $e) {
            $this->setLog($e);

            return $this->sendErrorMessage('Issue while creating user');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $userID
     * @return JsonResponse
     */
    public function show($userID)
    {
        try {
            return $this->sendSuccessMessage($this->pluginUserServices->show($userID), 'user found');
        } catch (\Exception $e) {
            $this->setLog($e);

            return $this->sendErrorMessage('No user found');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreUserRequest $request
     * @param User $user
     * @return JsonResponse|AnonymousResourceCollection
     */
    public function update(StoreUserRequest $request, User $user)
    {
        try {
            return $this->pluginUserServices->edit($request->validated(), $user);
        } catch (\Exception $e) {
            $this->setLog($e);

            return $this->sendErrorMessage('No user found');
        }
    }

    /**
     * Remove specified resource.
     *
     * @param int $id
     * @return int|JsonResponse
     */
    public function destroy(int $id)
    {
        try {
            return $this->pluginUserServices->delete($id);
        } catch (\Exception $e) {
            $this->setLog($e);

            return $this->sendErrorMessage('No user found');
        }
    }
}
