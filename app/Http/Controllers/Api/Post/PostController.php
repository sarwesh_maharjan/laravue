<?php

namespace App\Http\Controllers\Api\Post;

use App\Http\Controllers\BaseController;
use App\Services\PostServices;
use Illuminate\Http\JsonResponse;

class PostController extends BaseController
{
    /**
     * @var PostServices
     */
    protected $postServices;

    /**
     * PostController constructor.
     *
     * @param PostServices $postServices
     */
    public function __construct(PostServices $postServices)
    {
        $this->postServices = $postServices;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        try {
            return $this->postServices->allWithUser();
        } catch (\Exception $e) {
            $this->setLog($e);

            return $this->sendErrorMessage($e->getMessage());
        }
    }
}
