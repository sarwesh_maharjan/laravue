<?php

namespace App\Listeners;

use App\Events\UserHasBeenRegister;
use App\Notifications\UserRegistrationEmailNotifcation;

class WelcomeEmailListener
{
    /**
     * Handle the event.
     *
     * @param UserHasBeenRegister $event
     * @return void
     */
    public function handle(UserHasBeenRegister $event)
    {
        $event->user->notify(new UserRegistrationEmailNotifcation($event->user->toArray()));
    }
}
