<?php

namespace Database\Seeders;

use App\Models\Post\Post;
use App\Models\User;
use  Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        User::factory(50)->create();
        Post::factory(50)->create();
    }
}
