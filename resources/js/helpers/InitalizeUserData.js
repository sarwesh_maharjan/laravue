module.exports = {
    initialState() {
        return {
            errors: {},
            formData: {
                name: null,
                email: null,
                password: null,
                password_confirmation: null,
            },
        };
    },
};
